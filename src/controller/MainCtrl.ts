import { PanelCtrl } from 'grafana/app/plugins/sdk';
import _ from 'lodash';

import { SampleService } from 'services/SampleService';

export class MainCtrl extends PanelCtrl {
    //html template for Model panel
    static templateUrl = 'partials/module.html';

    //Panel Default Values
    panelDefaults = {
        text: 'Hello World',
    };

    //Custom Variable (must be declared in )
    oBlService: any;

    /** @ngInject */
    constructor($scope, $injector) {
        super($scope, $injector);
        _.defaults(this.panel, this.panelDefaults);


        //initialized property or variable here
        this.oBlService = new SampleService();

        this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
        this.events.on('data-received', this.onDataReceived.bind(this));
        this.events.on('data-snapshot-load', this.onDataReceived.bind(this));
        this.events.on('data-error', this.onDataError.bind(this));
        this.events.on('init-panel-actions', this.onInitPanelActions.bind(this));
    }

    onInitEditMode() {
        //this method is responsible for Rdit-on mode template
        this.addEditorTab('Options', `public/plugins/${this.pluginId}/partials/options.html`, 2);
    }

    onDataReceived(){
        console.log('onDataReceived');
        //insert method here
    }

    onInitPanelActions(){
        console.log('onInitPanelActions');
        //insert method here
    }

    render() {
        //renders the method here
        console.log('Render');
        return super.render();
    }

    onDataError(err: any) {
        //for Data Error
        console.log('onDataError', err);
    }
}